# 매뉴얼

## List&lt;T&gt;
Double-linked List로 구현되어 있습니다.
템플릿 클래스로 작성되었으며, C++ iterator 구현이 되어있어 range based for loop을 사용할 수 있습니다.

#### class 
List&lt;T&gt;::Node
- value: T 형의 데이터
- next: 다음 노드
- pre: 이전 노드

List&lt;T&gt;::iterator
- C++ iterator의 형식을 따른다.

#### function
List()
- 빈 리스트 생성

void push_front(T value)
- 리스트의 앞에 새 데이터를 삽입

void pop_front()
- 리스트의 앞 노드를 삭제

void push_back(T value)
- 리스트의 뒤에 새 데이터를 삽입

void pop_back()
- 리스트의 뒷 노드를 삭제

std::shared_ptr&lt;Node&gt; front() const
- 리스트의 가장 앞 노드를 반환

std::shared_ptr&lt;Node&gt; find(std::function&lt;bool(T&)&gt; find_expression) const
- 리스트에서 데이터를 검색하고 위치를 반환
- find_expression: 찾을 데이터가 맞을때 true를 리턴하는 함수

void clear()
- 리스트를 비운다.

iterator begin() const

iterator end() const

size_t size() const
- 리스트의 크기를 반환한다.


## Tree<T>
Red-Black Tree로 구현되어 있습니다.
템플릿 클래스로 작성되었으며, C++ iterator 구현이 되어있어 range based for loop을 사용할 수 있습니다.

#### class
Tree&lt;T&gt;::Node
- value: T 형의 데이터
- count: 중복된 데이터 개수
- parent: 부모 노드
- left: 왼쪽 자식 노드
- right: 오른쪽 자식 노드
- color: 노드의 색상

List&lt;T&gt;::iterator
- C++ iterator의 형식을 따른다.

#### function
Tree(std::function&lt;int(T&, T&)&gt; compare_func, bool allow_duplicated = false)
- 빈 트리 생성
- compare_func: 데이터를 비교하는 함수. 두 데이터가 같을때 0, 첫번째가 두번째보다 왼쪽에 들어갈 때 양수
- allow_duplicated: 중복을 허용하는지의 여부. 기본으로 false로 설정되어 있다.

void clear()
- 트리를 비운다.

bool insert(T value)
- 새로운 데이터를 삽입한다.

bool erase(T value, bool delete_all = false)
- 데이터를 삭제한다.
- delete_all: 중복된 데이터를 모두 지울건지의 여부. 기본으로 false로 설정되어 있다.

sptr&lt;Node&gt; find(T value) const
- 데이터를 찾아 노드의 주소를 반환한다.

List&lt;std::shared_ptr&lt;Node&gt;&gt; get_top(int n) const
- 트리의 데이터를 처음부터 끝까지 순서대로 리스트의 형태로 반환합니다.
- n: n개만큼 가져오고, n이 양수가 아니면 전체를 가져옵니다.

List&lt;std::shared_ptr&lt;Node&gt;&gt; get_bottom(int n) const
- 트리의 데이터를 끝부터 처음까지 역순대로 리스트의 형태로 반환합니다.
- n: n개만큼 가져오고, n이 양수가 아니면 전체를 가져옵니다.

std::shared_ptr&lt;Node&gt; get_first() const
- 트리의 가장 왼쪽 노드를 가져옵니다.

void debug_print(std::function&lt;void(T&)&gt; print_func) const
- 트리의 형태를 출력합니다.
- print_func: T형 데이터를 받아 출력하는 함수.

iterator begin() const

iterator end() const 

size_t size() const
- 트리의 크기를 반환합니다.


## Data
### User
User 데이터의 형식입니다.
- id: id 문자열
- name: 이름 문자열
- follower: 팔로워 목록을 담는 트리
- following: 팔로잉 목록을 담는 트리
- mentioned: 트윗한 단어 목록을 담는 트리
- graph_cost: 그래프 탐색에 쓰는 변수. 체크할 때나 그래프 가중치를 나타낼 때 사용.


### Word
Word 데이터의 형식입니다.
- content: 내용 문자열
- mentioning: 트윗한 유저 목록을 담는 트리


## main
메인 프로그램은 프로젝트에 명시된 기능을 수행합니다.

option 0. load_data()
- 데이터를 불러와 적절하게 저장합니다.

option 1. display_statistics()
- 데이터의 간략한 정보를 출력합니다.

option 2. display_top5_words()
- 가장 많이 트윗된 단어 5개를 출력합니다.

option 3. display_top5_users()
- 가장 많이 트윗한 유저 5명을 출력합니다.
- 결과는 홀더에 저장합니다. 홀더는 option 5에서 사용할 수 있습니다.

option 4. display_users_tweeted(std::string word)
- 입력으로 단어를 받고, 단어를 트윗한 모든 유저를 출력합니다.
- 결과는 홀더에 저장합니다. 홀더는 option 5에서 사용할 수 있습니다.

option 5. display_friend_of_above()
- 홀더에 저장된 유저의 팔로워를 전부 출력합니다.
- 두 명의 해당하는 유저를 팔로우하는 유저는 둘 중 한명만 팔로우하는 것처럼 단순하게 표현됩니다.

option 6. delete_word(std::string word)
- 입력으로 단어를 받고, 해당 단어를 삭제합니다.
- 단어를 삭제한 후 아무 트윗도 하지 않은 유저가 생겨도 유저를 삭제하지 않습니다.

option 7. delete_users_censored(std::string word)
- 입력으로 단어를 받고, 단어를 트윗한 모든 유저를 삭제합니다.
- 유저를 삭제한 후 아무도 트윗하지 않은 단어는 데이터에서 삭제합니다.

option 8. display_scc()
- 모든 Strongly Connected Components를 출력합니다.
- 하나의 SCC는 []로 묶여있습니다.

option 9. display_shortest_path(std::string name)
- 입력으로 유저 이름을 받고, 유저로부터 탐색을 시작해 가장 가중치가 작은 유저 5명을 출력합니다.
- 가중치는 유저의 팔로워 수이며, 방향 그래프는 팔로잉으로 표현됩니다.

option 99. exit
- 프로그램을 종료합니다.