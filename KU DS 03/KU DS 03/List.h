#pragma once
#include <utility>
#include <functional>
#include <memory>

template<typename T>
class List
{
	template<typename U> using sptr = std::shared_ptr<U>;

public:
	struct Node
	{
		T value;
		sptr<Node> next, pre;

		Node(T value): 
			value(value) {}
	};

	class iterator
	{
		friend class List;
		sptr<Node> pNode;

	public:
		T operator*() {
			return pNode->value;
		}

		iterator& operator++() {
			pNode = pNode->next;
			return *this;
		}

		iterator const operator++(int) {
			iterator temp = *this;
			pNode = pNode->next;
			return temp;
		}

		bool operator==(iterator const& it) const {
			return pNode == it.pNode;
		}

		bool operator!=(iterator const& it) const {
			return pNode != it.pNode;
		}
	};
	
private:
	sptr<Node> first, last;
	size_t count;

public:
	List(): count(0) {
	}

	~List() {
		clear();
	}

	void push_front(T value) {
		if ( count == 0 ) {
			first.reset(new Node(value));
			last = first;
		}
		else {
			sptr<Node> f = std::move(first);
			first.reset(new Node(value));
			first->next = f;
			f->pre = first;
		}
		++count;
	}

	bool pop_front() {
		if ( count == 0 )
			return false;
		else if ( count == 1 ) {
			first.reset();
			last.reset();
		}
		else {
			first = first->next;
			first->pre.reset();
		}
		--count;
		return true;
	}

	void push_back(T value) {
		if ( count == 0 ) {
			first.reset(new Node(value));
			last = first;
		}
		else {
			sptr<Node> pl = last;
			last->next.reset(new Node(value));
			last = last->next;
			last->pre = pl;
		}
		++count;
	}

	bool pop_back() {
		if ( count == 0 )
			return false;
		else if ( count == 1 ) {
			first.reset();
			last.reset();
		}
		else {
			last = last->pre;
			last->next.reset();
		}
		--count;
		return true;
	}

	sptr<Node> front() const {
		return first ? first : nullptr;
	}

	sptr<Node> find(std::function<bool(T&)> find_expression) const {
		for ( sptr<Node> c = first, n; c; c = n ) {
			n = c->next;
			if ( find_expression(c->value) )
				return c;
		}
		return nullptr;
	}
	
	void clear() {
		last.reset();
		while ( first ) {
			first->pre.reset();
			first = std::move(first->next);
		}
		count = 0;
	}

	iterator begin() const {
		iterator it;
		it.pNode = count > 0 ? first : nullptr;
		return it;
	}
	
	iterator end() const {
		iterator it;
		it.pNode = nullptr;
		return it;
	}

	size_t size() const { 
		return count; 
	}

	List(List& o) = delete;
	List& operator=(List&) = delete;
	List(List&& o): List() {
		std::swap(first, o.first);
		std::swap(last , o.last);
		std::swap(count, o.count);
	}
};
