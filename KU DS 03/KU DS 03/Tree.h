#pragma once
#include "List.h"
#include <cstdio>
#include <memory>

template<typename T>
class Tree
{
	template<typename U> using sptr = std::shared_ptr<U>;
	template<typename U> using wptr = std::weak_ptr<U>;

public:
	struct Node
	{
		friend class Tree;

		Tree* tree;
		T value;
		int count;
		sptr<Node> parent;
		sptr<Node> left, right;
		enum class Color { Black, Red } color;

		Node(Tree* tree, T value):
			tree(tree), value(value), color(Color::Red), count(1) {}
		
		bool operator==(Node const& n) const {
			return compare(value, n.value) == 0;
		}

		bool operator!=(Node const& n) const {
			return compare(value, n.value) != 0;
		}
	};

	class iterator
	{
		friend class Tree;
		sptr<Node> pNode;

	public:
		T operator*() {
			return pNode->value;
		}

		iterator& operator++() {
			if ( pNode->right ) {
				pNode = pNode->right;
				while ( pNode->left )
					pNode = pNode->left;
			}
			else {
				sptr<Node> p = pNode->parent;
				while ( p && pNode == p->right ) {
					pNode = p;
					p = p->parent;
				}
				pNode = p;
			}
			return *this;
		}

		iterator const operator++(int) {
			iterator temp = *this;
			if ( pNode->right ) {
				pNode = pNode->right;
				while ( pNode->left )
					pNode = pNode->left;
			}
			else {
				sptr<Node> p = pNode->parent;
				while ( p && pNode == p->right ) {
					pNode = p;
					p = p->parent;
				}
				pNode = p;
			}
			return temp;
		}

		bool operator==(iterator const& it) const {
			return pNode == it.pNode;
		}

		bool operator!=(iterator const& it) const {
			return pNode != it.pNode;
		}
	};
	
private:
	sptr<Node> root;
	size_t count;
	std::function<int(T& ,T&)> const compare;
	bool allow_duplicated;
	
public:
	Tree(std::function<int(T&, T&)> compare_func, bool allow_duplicated = false):
		root(nullptr), count(0), compare(compare_func), allow_duplicated(allow_duplicated) {
	}
	
	void clear() {
		if ( root == nullptr )
			return;
		List<sptr<Node>> all;
		all.push_back(std::move(root));
		while ( all.size() > 0 ) {
			sptr<Node> n = all.front()->value;
			all.pop_front();
			n->parent.reset();
			if ( n->left )
				all.push_back(std::move(n->left));
			if ( n->right )
				all.push_back(std::move(n->right));
		}
		count = 0;
	}

	~Tree() {
		clear();
	}

private:
	void rotate_left(sptr<Node> node) {
		//   t            c
		// 1   c   ->   t   3
		//    2 3      1 2
		sptr<Node> t = node;
		sptr<Node> c = node->right;
		sptr<Node> p = t->parent;
		sptr<Node> b = c->left;
		
		if ( b )
			b->parent = t;
		
		if ( p == nullptr )
			root = c;
		else if ( p->left == t )
			p->left = c;
		else
			p->right = c;

		t->right = b;
		t->parent = c;
		c->left = t;
		c->parent = p;
	}

	void rotate_right(sptr<Node> node) {
		//    t          c
		//  c   3  ->  1   t
		// 1 2            2 3
		sptr<Node> t = node;
		sptr<Node> c = node->left;
		sptr<Node> p = t->parent;
		sptr<Node> b = c->right;

		if ( b )
			b->parent = t;

		if ( p == nullptr )
			root = c;
		else if ( p->right == t )
			p->right = c;
		else
			p->left = c;

		t->left = b;
		t->parent = c;
		c->right = t;
		c->parent = p;
	}

	bool isRed(sptr<Node> node) const {
		return node && node->color == Node::Color::Red;
	}

	bool isBlack(sptr<Node> node) const {
		return node == nullptr || node->color == Node::Color::Black;
	}

	void setRed(sptr<Node> node) {
		if ( node )
			node->color = Node::Color::Red;
	}

	void setBlack(sptr<Node> node) {
		if ( node )
			node->color = Node::Color::Black;
	}

public:
	bool insert(T value) {
		sptr<Node> current = root;

		// binary tree insert
		if ( root == nullptr ) {
			// root is inserted -> done. pass
			root.reset(new Node(this, value));
			setBlack(root);
			++count;
			return true;
		}
		else {
			while ( current ) {
				int res = compare(current->value, value);
				if ( res == 0 ) {      // duplicated 
					if ( allow_duplicated ) {
						++current->count;
						++count;
						return true;
					}
					else
						return false;
				}
				else if ( res > 0 ) {  // value > current
					if ( current->left )
						current = current->left;
					else {
						current->left.reset(new Node(this, value));
						current->left->parent = current;
						current = current->left;
						break;
					}
				}
				else {                 // value < current
					if ( current->right )
						current = current->right;
					else {
						current->right.reset(new Node(this, value));
						current->right->parent = current;
						current = current->right;
						break;
					}
				}
			}
		}
		
		// RB fix
		// parent is Black -> done. pass
		while ( isRed(current->parent) ) {
			sptr<Node> parent = current->parent;
			sptr<Node> grand  = parent->parent;
			if ( parent == grand->left ) {
				sptr<Node> uncle  = grand->right;
				// case 1. parent and uncle are both Red
				if ( isRed(uncle) ) {
					setBlack(parent);
					setBlack(uncle);
					setRed  (grand);
					current = grand;
				}
				// parent is Red, uncle is Black
				else {
					// case 2. current and parent are diffrent direction -> rotate parent
					if ( current == parent->right ) {
						current = parent;
						rotate_left(current);
						parent = current->parent;
						grand = parent->parent;
						uncle = grand->right;
					}
					// case 3. current and parent are same direction -> rotate grand
					setBlack(parent);
					setRed  (grand);
					rotate_right(grand);
				}
			}
			else {
				sptr<Node> uncle  = grand->left;
				// case 1. parent and uncle are both Red
				if ( isRed(uncle) ) {
					setBlack(parent);
					setBlack(uncle);
					setRed  (grand);
					current = grand;
				}
				// parent is Red, uncle is Black
				else {
					// case 2. current and parent are diffrent direction -> rotate parent
					if ( current == parent->left ) {
						current = parent;
						rotate_right(current);
						parent = current->parent;
						grand = parent->parent;
						uncle = grand->left;
					}
					// case 3. current and parent are same direction -> rotate grand
					setBlack(parent);
					setRed  (grand);
					rotate_left(grand);
				}
			}
			setBlack(root);
		}

		++count;
		return true;
	}
	
private:
	void erase_fixup(sptr<Node> node, sptr<Node> parent) {
		// case 1. node is new root -> pass
		while ( node != root && isBlack(node) ) {
			if ( node == parent->left ) {
				sptr<Node> sible = parent->right;
				// case 2. sibling is red 
				if ( isRed(sible) ) {
					setBlack(sible);
					setRed(parent);
					rotate_left(parent);
					sible = parent->right;
				}
				// case 3. sibling, child of sibling is Black 
				if ( isBlack(sible->left) && isBlack(sible->right) ) {
					setRed(sible);
					node = parent;
					parent = node->parent;
				}
				else {
					// case 4. sibling is Black, node-direction child of sibling is Red, other child is Black
					if ( isBlack(sible->right) ) {
						setRed(sible);
						setBlack(sible->left);
						rotate_right(sible);
						sible = parent->right;
					}
					// case 6. sibling is Black, node-opposite-direction child of sibling is Red
					sible->color = parent->color;
					setBlack(parent);
					setBlack(sible->right);
					rotate_left(parent);
					node = root;
				}
			}
			else {
				sptr<Node> sible = parent->left;
				// case 2. sibling is red 
				if ( isRed(sible) ) {
					setBlack(sible);
					setRed(parent);
					rotate_right(parent);
					sible = parent->left;
				}
				// case 3. sibling, child of sibling is Black 
				if ( isBlack(sible->right) && isBlack(sible->left) ) {
					setRed(sible);
					node = parent;
					parent = node->parent;
				}
				else {
					// case 4. sibling is Black, node-direction child of sibling is Red, other child is Black
					if ( isBlack(sible->left) ) {
						setRed(sible);
						setBlack(sible->right);
						rotate_left(sible);
						sible = parent->left;
					}
					// case 6. sibling is Black, node-opposite-direction child of sibling is Red
					sible->color = parent->color;
					setBlack(parent);
					setBlack(sible->left);
					rotate_right(parent);
					node = root;
				}
			}
		}
		setBlack(node);
	}

	bool erase(sptr<Node> node, bool delete_all = false) {
		if ( node == nullptr )
			return false;

		if ( allow_duplicated && !delete_all && node->count > 1 ) {
			--node->count;
			--count;
			return true;
		}

		if ( node->left && node->right ) {
			sptr<Node> repl = node->right;
			while ( repl->left )
				repl = repl->left;
			std::swap(node->value, repl->value);
			node = repl;
		}

		sptr<Node> child, childParent;
		if ( node->left ) {
			child = node->left;
			childParent = node->parent;

			child->parent = childParent;
			if ( childParent == nullptr )
				root = child;
			else if ( childParent->left == node )
				childParent->left = child;
			else
				childParent->right = child;
		}
		else {
			child = node->right;
			childParent = node->parent;

			if ( child )
				child->parent = childParent;
			if ( childParent == nullptr )
				root = child;
			else if ( childParent->left == node )
				childParent->left = child;
			else
				childParent->right = child;
		}

		node->parent.reset();
		node->left.reset();
		node->right.reset();

		// RB fix
		if ( isBlack(node) )
			erase_fixup(child, childParent);
		
		count -= node->count;
		return true;
	}
public:
	bool erase(T value, bool delete_all = false) {
		return erase(find(value), delete_all);
	}

	sptr<Node> find(T value) const {
		sptr<Node> current = root, post;
		while ( current ) {
			post = current;
			int res = compare(current->value, value);
			if ( res == 0 )
				return current;
			else if ( res > 0 )  // value > current
				current = current->left;
			else                 // value < current
				current = current->right;
		}
		return nullptr;
	}

	List<sptr<Node>> get_top(int n) const {
		if ( root == nullptr )
			return List<sptr<Node>>();

		if ( n <= 0 )
			n = 0x7fffffff;

		struct Check
		{
			sptr<Node> value;
			int check;
			Check(sptr<Node> value): value(value), check(0) {}
		};

		List<sptr<Node>> result;
		List<Check> stack;

		stack.push_front(Check(root));

		while ( n > 0 && stack.size() > 0 ) {
			Check& t = stack.front()->value;
			if ( t.check == 0 ) {
				++(t.check);
				if ( t.value->left ) {
					stack.push_front(Check(t.value->left));
					continue;
				}
			}
			if ( t.check == 1 ) {
				result.push_back(t.value);
				--n;
				++(t.check);
				if ( t.value->right ) {
					stack.push_front(Check(t.value->right));
					continue;
				}
			}
			else {
				stack.pop_front();
			}
		}

		return result;
	}

	List<sptr<Node>> get_bottom(int n) const {
		if ( root == nullptr )
			return List<sptr<Node>>();

		if ( n <= 0 )
			n = 0x7fffffff;

		struct Check
		{
			sptr<Node> value;
			int check;
			Check(sptr<Node> value): value(value), check(0) {}
		};

		List<sptr<Node>> result;
		List<Check> stack;

		stack.push_front(Check(root));

		while ( n > 0 && stack.size() > 0 ) {
			Check& t = stack.front()->value;
			if ( t.check == 0 ) {
				++(t.check);
				if ( t.value->right ) {
					stack.push_front(Check(t.value->right));
					continue;
				}
			}
			if ( t.check == 1 ) {
				result.push_back(t.value);
				--n;
				++(t.check);
				if ( t.value->left ) {
					stack.push_front(Check(t.value->left));
					continue;
				}
			}
			else {
				stack.pop_front();
			}
		}

		return result;
	}

	sptr<Node> get_first() const {
		if ( root == nullptr )
			return nullptr;
		sptr<Node> t = root;
		while ( t->left )
			t = t->left;
		return t;
	}

	void debug_print(std::function<void(T&)> print_func) const {
		if ( root == nullptr ) {
			printf("null\n");
			return;
		}

		struct Check
		{
			sptr<Node> value;
			int check;
			bool right;
			Check(sptr<Node> value, bool right): value(value), check(0), right(right) {}
		};

		List<Check> stack;

		stack.push_front(Check(root, false));

		while ( stack.size() > 0 ) {
			Check& t = stack.front()->value;
			if ( t.check == 0 ) {
				++(t.check);
				if ( t.value->right ) {
					stack.push_front(Check(t.value->right, true));
					continue;
				}
			}
			if ( t.check == 1 ) {
				for ( int i = 1; i < stack.size(); ++i )
					printf("    ");
				if ( stack.size() > 1 )
					printf("%s", t.right? "��":"��");
				printf("%c:", isRed(t.value)?'R':'B');
				print_func(t.value->value);
				++(t.check);
				if ( t.value->left ) {
					stack.push_front(Check(t.value->left, false));
					continue;
				}
			}
			else {
				stack.pop_front();
			}
		}
	}

	iterator begin() const {
		iterator it;
		it.pNode = root;
		if ( root )
			while ( it.pNode->left )
				it.pNode = it.pNode->left;
		return it;
	}

	iterator end() const {
		iterator it;
		it.pNode = nullptr;
		return it;
	}

	size_t size() const {
		return count;
	}

	Tree(Tree& o) = delete;
	Tree& operator=(Tree&) = delete;
	Tree(Tree&& o): Tree(o.compare) {
		std::swap(root, o.root);
		std::swap(count, o.count);
	}
};
