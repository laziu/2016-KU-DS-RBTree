#pragma warning(disable:4996)
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include "Tree.h"
#include "Data.h"


// input method
char buffer[1001];

std::string input(std::string message) {
	printf("%s", message.c_str());
	scanf("%1000s", buffer);
	return std::string(buffer);
};

char* finput(FILE* f) {
	char* r = fgets(buffer, sizeof(buffer), f);
	int i;
	for ( i = 0; i < sizeof(buffer) && buffer[i]; ++i );
	for ( --i; i >= 0 && (buffer[i] == '\n' || buffer[i] == '\r' || buffer[i] == ' '); --i )
		buffer[i] = '\0';
	return r;
}


// data structure to solve the problem
List<sptr<User>> resultUserHolder;

Tree<sptr<User>> userIdTree     ([](sptr<User>& a, sptr<User>& b) { return b->id.compare(a->id); });
Tree<sptr<User>> userNameTree   ([](sptr<User>& a, sptr<User>& b) { return a->name.compare(b->name); });
Tree<sptr<Word>> wordContentTree([](sptr<Word>& a, sptr<Word>& b) { return a->content.compare(b->content); });

// option 0.
void load_data() {
	// clear data
	userIdTree.clear();
	userNameTree.clear();
	wordContentTree.clear();
	long long total_friends = 0, total_tweets = 0;

	// read user file
	printf(" reading user.txt ... ");
	FILE *file = fopen("user.txt", "r");
	while ( finput(file) ) {
		std::string id(buffer);

		finput(file);  // date, skip
		
		finput(file);
		std::string name(buffer);
		
		finput(file);  // blank, skip
		
		sptr<User> u(new User(id, name));
		userIdTree.insert(u);
		userNameTree.insert(u);
	}
	fclose(file);
	puts("done.");
	
	// read word file
	printf(" reading word.txt ... ");
	file = fopen("word.txt", "r");
	while ( finput(file) ) {
		std::string id(buffer);

		finput(file);  // date, skip
		
		finput(file);
		std::string content(buffer);
		
		finput(file);  // blank, skip
		
		sptr<Word> w(new Word(content));
		
		auto nu = userIdTree.find(sptr<User>(new User(id, "")));
		sptr<User> u = nu->value;
		
		if ( u == nullptr ) {
			continue;
		}

		++total_tweets;

		if ( content.compare("�װ�") == 0 ) {
			--total_tweets;
			++total_tweets;
		}

		auto wpos = wordContentTree.find(w);
		if ( wpos == nullptr ) {
			wordContentTree.insert(w);
			wpos = wordContentTree.find(w);
		}
		
		wpos->value->mentioning.insert(u);
		nu  ->value->mentioned .insert(w);
	}
	fclose(file);
	puts("done.");

	// read friend file
	printf(" reading friend.txt ... ");
	file = fopen("friend.txt", "r");
	while ( finput(file) ) {
		std::string fromid(buffer);
		
		finput(file);
		std::string toid(buffer);

		finput(file);  // blank, skip

		auto fwing = userIdTree.find(sptr<User>(new User(fromid, "")));
		auto fwer  = userIdTree.find(sptr<User>(new User(toid  , "")));

		if ( fwing && fwer ) {
			auto i = fwing->value->following.find(fwer->value);
			auto j = fwer ->value->follower .find(fwing->value);
			if ( !i && !j ) {
				fwing->value->following.insert(fwer->value);
				fwer->value->follower.insert(fwing->value);
				++total_friends;
			}
		}
	}
	fclose(file);
	puts("done.\n");

	// print some info
	printf(" Total users: %d\n", userIdTree.size());
	printf(" Total friendship records: %lld\n", total_friends);
	printf(" Total tweets: %lld\n", total_tweets);
}


// option 1.
void display_statistics() {
	long long all_friends = 0;
	long long min_friends = 0x7fffffff;
	long long max_friends = 0;
	long long all_tweets = 0;
	long long min_tweets = 0x7fffffff;
	long long max_tweets = 0;

	for ( auto i : userIdTree ) {
		long long friends = i->follower.size();
		long long tweets  = i->mentioned.size();
		all_friends += friends;
		if ( min_friends > friends )
			min_friends = friends;
		if ( max_friends < friends )
			max_friends = friends;
		all_tweets += tweets;
		if ( min_tweets > tweets )
			min_tweets = tweets;
		if ( max_tweets < tweets )
			max_tweets = tweets;
	}
	printf(" Total   number of friends: %lld\n", all_friends);
	printf(" Average number of friends: %lf\n", double(all_friends) / userIdTree.size());
	printf(" Minimum number of friends: %lld\n", min_friends);
	printf(" Maximum number of friends: %lld\n", max_friends);
	printf("\n");
	printf(" Total   tweets per user: %lld\n", all_tweets);
	printf(" Average tweets per user: %lf\n", double(all_tweets) / userIdTree.size());
	printf(" Minimum tweets per user: %lld\n", min_tweets);
	printf(" Maximum tweets per user: %lld\n", max_tweets);
}


// option 2.
void display_top5_words() {
	struct Res {
		sptr<Word> word;
		long long count;
		Res(): count(0) {}
	} top5[5];
	for ( auto i : wordContentTree.get_top(0) ) {
		auto iment = i->value->mentioning.size();
		for ( int j = 0; j < 5; ++j ) 
			if ( top5[j].word == nullptr || top5[j].count < iment ) {
				for ( int k = 4; k > j; --k )
					top5[k] = top5[k - 1];
				top5[j].word = i->value;
				top5[j].count = iment;
				break;
			}
	}
	if ( top5[0].word ) printf(" 1st: %-32s\t(%lld time tweeted)\n", top5[0].word->content.c_str(), top5[0].count);
	if ( top5[1].word ) printf(" 2nd: %-32s\t(%lld time tweeted)\n", top5[1].word->content.c_str(), top5[1].count);
	if ( top5[2].word ) printf(" 3rd: %-32s\t(%lld time tweeted)\n", top5[2].word->content.c_str(), top5[2].count);
	if ( top5[3].word ) printf(" 4th: %-32s\t(%lld time tweeted)\n", top5[3].word->content.c_str(), top5[3].count);
	if ( top5[4].word ) printf(" 5th: %-32s\t(%lld time tweeted)\n", top5[4].word->content.c_str(), top5[4].count);
}


// option 3.
void display_top5_users() {
	struct Res {
		sptr<User> user;
		long long count;
		Res(): count(0) {}
	} top5[5];
	for ( auto i : userIdTree.get_top(0) ) {
		auto iment = i->value->mentioned.size();
		for ( int j = 0; j < 5; ++j )
			if ( top5[j].user == nullptr || top5[j].count < iment ) {
				for ( int k = 4; k > j; --k )
					top5[k] = top5[k - 1];
				top5[j].user = i->value;
				top5[j].count = iment;
				break;
			}
	}
	if ( top5[0].user ) printf(" 1st: %-9s: %-32s\t(%lld time tweeted)\n", top5[0].user->id.c_str(), top5[0].user->name.c_str(), top5[0].count);
	if ( top5[1].user ) printf(" 2nd: %-9s: %-32s\t(%lld time tweeted)\n", top5[1].user->id.c_str(), top5[1].user->name.c_str(), top5[1].count);
	if ( top5[2].user ) printf(" 3rd: %-9s: %-32s\t(%lld time tweeted)\n", top5[2].user->id.c_str(), top5[2].user->name.c_str(), top5[2].count);
	if ( top5[3].user ) printf(" 4th: %-9s: %-32s\t(%lld time tweeted)\n", top5[3].user->id.c_str(), top5[3].user->name.c_str(), top5[3].count);
	if ( top5[4].user ) printf(" 5th: %-9s: %-32s\t(%lld time tweeted)\n", top5[4].user->id.c_str(), top5[4].user->name.c_str(), top5[4].count);
	for ( int i = 0; i < 5; ++i )
		resultUserHolder.push_back(top5[i].user);
}


// option 4.
void display_users_tweeted(std::string word) {
	if ( auto pos = wordContentTree.find(sptr<Word>(new Word(word))) ) {
		for ( auto i : pos->value->mentioning.get_top(0) ) {
			printf(" %-9s: %s\n", i->value->id.c_str(), i->value->name.c_str());
			resultUserHolder.push_back(i->value);
		}
	}
	else 
		printf(" tweet \"%s\" is not exists.\n", word.c_str());
}


// option 5.
void display_friend_of_above() {
	if ( resultUserHolder.size() <= 0 ) {
		puts("None of users on above.");
		return;
	}

	struct Res
	{
		sptr<User> user;
		sptr<User> follower;
		Res(sptr<User> u, sptr<User> f): user(u), follower(f) {}
	};

	Tree<Res> resultTree([](Res a, Res b) { return b.user->id.compare(a.user->id); });
	for ( auto i : resultUserHolder ) 
		for ( auto j : i->follower.get_top(0) ) 
			resultTree.insert(Res(j->value, i));

	if ( resultTree.size() <= 0 ) 
		puts("No follower exists.");
	else
		for ( auto i : resultTree.get_top(0) )
			printf(" %-9s: %-32s\t(following %-9s: %-16s)\n", 
				i->value.user->id.c_str(), i->value.user->name.c_str(), 
				i->value.follower->id.c_str(), i->value.follower->name.c_str());

	resultUserHolder.clear();
}


// option 6.
void delete_word(std::string word) {
	if ( auto nw = wordContentTree.find(sptr<Word>(new Word(word))) ) {
		auto w = nw->value;
		for ( auto i : w->mentioning.get_top(0) )
			i->value->mentioned.erase(w, true);
		wordContentTree.erase(w);
	}
	else
		printf(" word \"%s\" is not exists.\n", word.c_str());
}


// option 7.
void delete_users_censored(std::string word) {
	if ( auto nw = wordContentTree.find(sptr<Word>(new Word(word))) ) {
		Tree<sptr<User>> user_delete_list([](sptr<User> a, sptr<User> b) { return a->id.compare(b->id); });

		for ( auto i : nw->value->mentioning.get_top(0) )
			if ( auto u = i->value )
				user_delete_list.insert(u);

		for ( auto un : user_delete_list.get_top(0) ) {
			auto u = un->value;
			for ( auto fn : u->follower.get_top(0) )
				if ( auto f = fn->value )
					f->following.erase(u);
			for ( auto fn : u->following.get_top(0) )
				if ( auto f = fn->value )
					f->follower.erase(u);
			for ( auto wn : u->mentioned.get_top(0) )
				if ( auto w = wn->value ) {
					w->mentioning.erase(u, true);
					if ( w->mentioning.size() == 0 )
						wordContentTree.erase(w);
				}
			userNameTree.erase(u);
			userIdTree.erase(u);
		}
	}
	else
		printf(" word \"%s\" is not exists.\n", word.c_str());
}


// option 8.
void display_scc() {
	for ( auto i : userIdTree )
		i->graph_cost = 0;

	List<sptr<User>> dfslist;

	struct Elem
	{
		sptr<User> elem;
		Tree<sptr<User>>::iterator it;
		Elem(sptr<User> e, Tree<sptr<User>>::iterator i): elem(e), it(i) {}
	};

	for ( auto i : userIdTree ) {
		if ( i->graph_cost == 0 ) {
			i->graph_cost = 1;
			List<Elem> stack;
			stack.push_front(Elem(i, i->following.begin()));
			while ( stack.size() > 0 ) {
				sptr<User> u = stack.front()->value.elem;
				bool called = false;
				for ( auto& i = stack.front()->value.it; i != userIdTree.end(); ++i ) {
					if ( (*i)->graph_cost == 0 ) {
						(*i)->graph_cost = 1;
						stack.push_front(Elem(*i, (*i)->following.begin()));
						called = true;
						break;
					}
				}
				if ( !called ) {
					u->graph_cost = 2;
					dfslist.push_front(u);
					stack.pop_front();
				}
			}
		}
	}

	for ( auto i : dfslist ) {
		if ( i->graph_cost <= 2 ) {
			printf("[");
			int ccc = 0;
			i->graph_cost = 3;
			List<sptr<User>> queue;
			queue.push_back(i);
			while ( queue.size() > 0 ) {
				sptr<User> u = queue.front()->value;
				for ( auto& i : u->follower ) {
					if ( i->graph_cost <= 2 ) {
						i->graph_cost = 3;
						queue.push_back(i);
					}
				}
				u->graph_cost = 4;
				printf("%s%-20s", ccc++ ? ", " : "", u->name.c_str());
				queue.pop_front();
			}
			puts("]");
		}
	}
}


// option 9.
void display_shortest_path(std::string name) {
	auto upos = userNameTree.find(sptr<User>(new User("", name)));
	if ( upos == nullptr || upos->value == nullptr ) {
		printf(" user \"%s\" is not exists.\n", name.c_str());
		return;
	}

	for ( auto i : userIdTree )
		i->graph_cost = 0x7fffffff;
	
	Tree<sptr<User>> set([](sptr<User>& a, sptr<User>& b) { return a->id.compare(b->id); });

	upos->value->graph_cost = -1;
	for ( auto i : upos->value->following ) {
		i->graph_cost = i->follower.size();
		set.insert(i);
	}

	int print_count = 0;
	while ( print_count < 5 && set.size() > 0 ) {
		sptr<User> u = set.get_first()->value;
		for ( auto& i : set )
			if ( u->graph_cost > i->graph_cost )
				u = i;
		set.erase(u);
		if ( u->graph_cost == -1 )
			continue;
		switch ( ++print_count ) {
		case 1: printf(" 1st: "); break;
		case 2: printf(" 2nd: "); break;
		case 3: printf(" 3rd: "); break;
		case 4: printf(" 4th: "); break;
		case 5: printf(" 5th: "); break;
		}
		printf("%3d - %-9s: %s\n", u->graph_cost, u->id.c_str(), u->name.c_str());
		for ( auto i : u->following ) {
			if ( i->graph_cost > u->graph_cost + i->follower.size() ) {
				i->graph_cost = u->graph_cost + i->follower.size();
				set.insert(i);
			}
		}
		u->graph_cost = -1;
	}
}


int main(void) {
	while ( true ) {
		// print interface
		puts("0. Read data files");
		puts("1. display statistics");
		puts("2. Top 5 most tweeted words");
		puts("3. Top 5 most tweeted users (and hold)");
		puts("4. Find users who tweeted a word (and hold)");
		puts("5. Find all people who are friends of the above users");
		puts("6. Delete all mentions of a word");
		puts("7. Delete all users who mentioned a word");
		puts("8. Find strongly connected components");
		puts("9. Find shortest path from a given user");
		puts("99. Quit");
		puts("");
		if ( resultUserHolder.size() > 0 ) {
			printf("Holding users: [");
			int i = 0;
			for ( auto c : resultUserHolder ) {
				printf("%s%s", (i++ > 0 ? ", " : ""), c->name.c_str());
			}
			puts("]\n");
		}

		// get options input
		int arg;
		while ( true ) {
			printf("Select Menu: ");
			if ( scanf("%d", &arg) != 1 ) {
				puts("Option should be integer, type again.");
				scanf("%1000s", buffer);
			}
			else if ( 0 <= arg && arg <= 9 || arg == 99 )
				break;
			else {
				puts("Invalid Option, type again.");
			}
		}
		puts("");

		// branch options
		if ( arg != 5 )
			resultUserHolder.clear();

		switch ( arg ) {
		case 0: load_data();										break;
		case 1: display_statistics();								break;
		case 2: display_top5_words();								break;
		case 3: display_top5_users();								break;
		case 4: display_users_tweeted(input("Enter word: "));		break;
		case 5: display_friend_of_above();							break;
		case 6: delete_word(input("Enter word: "));					break;
		case 7: delete_users_censored(input("Enter word: "));		break;
		case 8: display_scc();										break;
		case 9: display_shortest_path(input("Enter user name: "));	break;
		case 99: return 0;
		}

		// press to continue and clear screen
		puts("");
		puts("------------------------------------------------------------");
		puts("");
		puts("Press ENTER to continue...");

		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin.get();
#ifdef _WIN32
		system("cls");
#else
		system("clear");
#endif
	}

	return 0;
}
