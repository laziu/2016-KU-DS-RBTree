#include "Data.h"

User::User(std::string id, std::string name):
	id(id), name(name), 
	follower ([](sptr<User> a, sptr<User> b) { return a->id     .compare(b->id); }),
	following([](sptr<User> a, sptr<User> b) { return a->id     .compare(b->id); }),
	mentioned([](sptr<Word> a, sptr<Word> b) { return a->content.compare(b->content); }, true) {
}

Word::Word(std::string content): content(content), 
	mentioning([](sptr<User> a, sptr<User> b) { return a->id.compare(b->id); }, true) {
}
