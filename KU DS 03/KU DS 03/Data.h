#pragma once
#include <string>
#include "Tree.h"
#include <memory>

template<typename U> using sptr = std::shared_ptr<U>;
template<typename U> using wptr = std::weak_ptr<U>;

struct Word;

struct User
{
	std::string id;
	std::string name;
	
	Tree<sptr<User>> follower;
	Tree<sptr<User>> following;
	Tree<sptr<Word>> mentioned;

	int graph_cost;
	
	User(std::string id, std::string name);
};


struct Word
{
	std::string content;
	
	Tree<sptr<User>> mentioning;

	Word(std::string content);
};