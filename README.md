# 2016 KU DS 03
COSE213-03 2016 1학기 과제

###Environment
- Visual C++ 2015 on Windows 10

##Report
- What data structure you chose and why

  유저와 단어의 데이터를 RB트리로 들고 있게 했으며, 유저는 팔로잉 트리, 팔로워 트리, 멘션 트리를 가지고 있고 단어는 멘션한 유저 트리를 갖고 있습니다. 
  이를 통해 팔로잉 그래프와 유저 id 트리를 동시에 사용할 수 있으며, 유저와 단어 관계 트리를 이용해 각 문제를 효과적으로 풀 수 있습니다.


- What is your expected performance

  option 0: O(n log n)
  - 각각의 user마다 tree insert 2번
  - 각각의 word마다 tree find 2번, insert 2번
  - 각각의 friend마다 tree find 4번, insert 2번
  
  option 1: O(n)
  - 트리 전위탐색 1번

  option 2: O(n)
  - 트리 전위탐색 1번, 각 Word마다 비교 및 삽입 5번

  option 3: O(n)
  - 트리 전위탐색 1번, 각 User마다 비교 및 삽입 5번

  option 4: O(n)
  - 트리 find 1번, 전위탐색 1번

  option 5: O(n^2)
  - 각각의 유저마다 트리 전위탐색 1번, 전체 트리탐색 1번

  option 6: O(n log n)
  - 트리 find 1번, 전위탐색 1번, 각각마다 삭제 1번, 마지막 삭제 1번

  option 7: O(n^2 log n)
  - 트리 전위탐색 1번, 각각마다 전위탐색 3번, 기타 삭제 및 탐색

  option 8: O(n)
  - 트리 전위탐색 3번

  option 9: O(n^2)
  - 다익스트라 알고리즘, priority_queue 구현 안하고 그냥 queue로 함


- How would you improve the system in the future

  트리 데이터를 삭제할 때 메모리 해제가 제대로 이루어지지 않는다. 메모리 해제를 제대로 수행할 수 있도록 개선할 수 있을 것이다.

  option 7에서 시간을 많이 사용한다. User와 Word가 각각을 참조하는 모든 트리 노드 위치를 저장하고 있으면 탐색을 할 필요가 없으므로 시간복잡도가 줄어들 것이다.


##Self evaluation form
- Submit a github account: 10/10
- Commit source code displaying menu: 10/10
- Commit the first draft of manual: 10/10
- Read data files: 20/20
- Statistics: 20/20
- Top 5 most tweeted words: 10/10
- Top 5 most tweeted users: 5/5
- Find all users who mentioned a word: 10/10
- Find all users who are friend of the above user: 5/5
- Find shortest path from a user: 10/10. 참고로 id가 아니라 name으로 찾도록 구현함.

